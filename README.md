Задание №4. Примитивные типы данных. Числа.

Реализуем решение квадратного уравнения 

2.1. Решение должно быть представлено в виде статического метода класса
public class SquareEquation
{
  public static double[] Solve(double a, double b, double c)
  {
     // здесь пишется решение уравнения
  }
}

2.2. Используем формулы (2.12) на стр. 20 из  http://mech.math.msu.su/~iliagri/zip/sem2book.pdf.

2.3. Если коэффициент a = 0 квадратного уравнения, то метод должен выбросить исключение System.ArgumentException. Синтаксис исключений и их назначение можно прочитать по ссылке https://docs.microsoft.com/ru-ru/dotnet/csharp/fundamentals/exceptions/ (читаем весь раздел)

2.4. Коэффициенты a, b, c могут принимать не только числовые значения, но и NaN, +/-infinity. В случае перечисленных значений также нужно выбросить исключение ArgumentException.

2.5. Необходимо учесть, что код из примера на стр. 20
if (D < 0) return 0;
не учитывает ситуацию |D| < eps - это ноль. Следовательно, благодаря этому if процедура kvadr будет выдавать отсутствие корней для случаев -eps < D < 0, когда на самом деле есть два корня кратности 1.

2.6. В случае, когда корней нет, метод Solve возвращает пустой массив.
На основе класса Solve реализуем остальное приложение, которое принимает на на вход коэффициенты квадратного уравнения a, b, c d качестве параметров командной строки и выводит на консоль:
Уравнение:{a}*x^2 + {b}*x + {c} = 0
Корень x_1 = {x_1} невязка {значение {a}*{x_1}^2 + {b}*{x_1} + {c}}
Корень x_2 = {x_2} невязка {значение {a}*{x_2}^2 + {b}*{x_2} + {c}}

либо
Уравнение:{a}*x^2 + {b}*x + {c} = 0
Корней нет

Либо 
Неверные параметры квадратного уравнения

где {a}, {b}, {c} - значения коэффициентов, которые переданы в приложение,
{x_1}, {x_2} - значение корней,
{значение {a}*{x_1}^2 + {b}*{x_1} + {c}}, {значение {a}*{x_2}^2 + {b}*{x_2} + {c}} - значение невязки 


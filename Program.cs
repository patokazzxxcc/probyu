﻿namespace lab4

{

    public class SquareEquation

    {

        public static double[] Solve(double a, double b, double c)

        {

            double x1, x2, d = b * b - 4 * a * c;

            const double eps = 1e-5;

            if (Math.Abs(a) < eps  || Double.IsInfinity (a) || Double.IsInfinity (b)

            || Double.IsInfinity (c) || Double.IsNaN (a) || Double.IsNaN (b) || Double.IsNaN (c))
            
            {

                throw new ArgumentException();

            }

            if (Math.Abs(d) < eps)

            {

                x1 = -b / (2 * a);

                return new double[] { x1, x1 };

            }

            if (d < -eps)

            {

                return new double[] { };

            }

            if (Math.Abs(b) < eps && Math.Abs(c) < eps)

            {

                return new double[] { 0, 0 };

            }

            if (Math.Abs(b) < eps)

            {

                x1 = Math.Sqrt(d) / (2 * a);

                x2 = c / (a * x1);

                return new double[] { x1, x2 };
                
            }

            if (Math.Abs(c) < eps) 

            {

                return new double[] { 0, -b / a };

            }

            x1 = -(b + Math.Sign(b) * Math.Sqrt(d)) / (2 * a);

            x2 = c / (a * x1);

            return new double[] { x1, x2 };

        }

    }


    class Kvadri_yrav

    {

        static void Main(string[] args)

        {

            double a = Double.Parse(Environment.GetCommandLineArgs()[1], System.Globalization.CultureInfo.InvariantCulture);
            
            double b = Double.Parse(Environment.GetCommandLineArgs()[2], System.Globalization.CultureInfo.InvariantCulture);
            
            double c = Double.Parse(Environment.GetCommandLineArgs()[3], System.Globalization.CultureInfo.InvariantCulture);
            
            try

            {
                
                double[] result = new double[2];
                
                result = SquareEquation.Solve(a, b, c);
                
                Console.WriteLine($"Уравнение: {a}*x^2 + {b}*x + {c} = 0");

                if (result.Length == 0)

                {

                    Console.WriteLine("Корней нет");

                }

                else

                {

                    Console.WriteLine($"Корень x1 = {result[0]} невязка {a * result[0] * result[0] + b * result[0] + c} ");

                    Console.WriteLine($"Корень x2 = {result[1]} невязка {a * result[1] * result[1] + b * result[1] + c} ");
                
                }
                
            }

            catch (ArgumentException)

            {

                Console.WriteLine("Неверные параметры квадратного уравнения");

            }

        }

    }

}